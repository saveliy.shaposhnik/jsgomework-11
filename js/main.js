const btns = document.getElementsByClassName("btn");

for (let index = 0; index < btns.length; index++) {
  btns[index].classList.remove("active");
  document.addEventListener("keydown", function (e) {
    for (let index = 0; index < btns.length; index++) {
      btns[index].classList.remove("active");
      if (e.key === btns[index].textContent || e.key === btns[index].textContent.toLowerCase()) {
        btns[index].classList.add("active");
      }
    }
  });
}
